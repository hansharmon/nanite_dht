# README #

This is a nanite wrapper code for the dht11 and dht22

This repository is a refactor of the adafruit code for the dht11 and dht22. 
https://github.com/adafruit/Adafruit_Python_DHT/

The refactor is to take the code an put it into a form that matches nanite outputs instead of being out to screen.  Most of the code will be copied verbatum.

Make sure to have nanite installed (or the docker if you want to go that route).

https://bitbucket.org/hansharmon/nanite/src/master/

Useage:  
   mkdir build
   cd build
   cmake ..
   make
   sudo make install
   cd ../example/
   nanite dht11.json