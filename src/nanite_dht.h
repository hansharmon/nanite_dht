#ifndef NANITE_DHT_H
#define NANITE_DHT_H

#include <nanite/nanite_measure.h>


namespace nanite {
  class nanite_dht :
    public nanite::nanite_measure {                    //!< Super class nanite_measure
      public:
        nanite_dht();
        ~nanite_dht();

        void init();

        // From the measure class.  Temperature is the 'value' of the measurement.
        void measure();


      protected:
        rx_int dht_type;    //!< Type either 11 or 22
        rx_int pin;     //!< GPIO Pin (on the PI) for the device.  TODO:  Check if we can make this work with gpiod?

        rx_float temperature;   //!< Temperature in C
        rx_float humidity;      //!< Humidity in Percent.
  };

};


#endif // NANITE_DHT_H
