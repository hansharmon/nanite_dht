#include "nanite_dht.h"


#include <unistd.h>
#include <stdio.h>

extern "C" {
#include "pi_2_dht_read.h"
}
namespace nanite {
  nanite_dht::nanite_dht() : nanite_measure("DHT") {
    dht_type=11;
    add_int("dht_type",dht_type);

    pin=5;
    add_int("pin",pin);


    temperature=0;
    add_float("temperature",temperature);

    humidity=0;
    add_float("humidity",humidity);
  }

  nanite_dht::~nanite_dht() {
  }

  void nanite_dht::init() {
  }

  void nanite_dht::measure() {
    float h;
    float t;
    int rv = pi_2_dht_read((int) dht_type,(int) pin,&h,&t);
    if (rv == DHT_SUCCESS) {
      temperature = t;
      humidity = h;
    } else {
      log = "Error Reading DHT: " + to_string(rv);
    }
  }

};

NANITE_GEN_PROXY(DHT,nanite::nanite_dht)
