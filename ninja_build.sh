#!/bin/bash
##############################################
# Ninja Build
#
# This script is used to build using ninja instead of make
# This will decrease the rebuild time considerably
##############################################

set -x
if [ -z $ARCH ]
    then
        ARCH=linux
fi

if [ -z "$1" ]
    then
        directory=build_$ARCH
fi

rm -rf $directory
rm -rf output/$ARCH
mkdir $directory
cd $directory
cmake -GNinja -DCMAKE_BUILD_TYPE="Release" ..
ninja -j$(nproc)
cpack

