#!/bin/bash
set -x
directory=build_$1

rm -rf $directory
mkdir $directory
cd $directory
cmake -DCMAKE_BUILD_TYPE="Release" ..
make -j$(nproc)
